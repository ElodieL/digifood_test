import React, { useContext } from 'react';
import { ProductContext } from '../../contexts/ProductContext';
import Product from '../Product/Product';
import './ProductList.scss';
import { ActivatedTabContext } from '../../contexts/ActivatedTabContext';


const ProductList = () => {
    const [productLists] = useContext(ProductContext);
    const [activatedTab] = useContext(ActivatedTabContext);

    return ( 
        <div className="Content">
            {productLists.categories.map(category => 
            <div key={category.name}>
                {activatedTab === category.name  &&
                <div>
                    <ul className="Content__Tabs">
                    {category.products.map(product =>
                        <li className="Content__Items" key={product.id}>
                            <Product 
                                id={product.id}
                                name={product.name}
                                price={product.price}
                                image={product.image}>
                            </Product>
                        </li>
                    )}
                    </ul>
                </div>
                }
            </div>
            )}
        </div>
    );
}
 
export default ProductList;