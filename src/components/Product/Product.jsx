import React, { useContext } from 'react';
import { ShoppingListContext } from '../../contexts/ShoppingListContext';
import { TotalPriceContext } from '../../contexts/TotalPriceContext';
import './Product.scss';
import addCartIcon from './../../assets/SVGIcons/add_shopping_cart-24px.svg';

const Product = (product) => {
    const [shoppingList, setShoppingList] = useContext(ShoppingListContext);
    const [totalPrice, setTotalPrice] = useContext(TotalPriceContext);

    const randomInt = (min = 10000, max = 99999) => {
            return min + Math.floor((max - min) * Math.random())
        }

    
    const addProductIntoShoppingList = product => {

        const addedProduct = { 
            shopId: randomInt(),
            quantity: 1,
            id: product.id,
            name: product.name,
            price: product.price,
            image: product.image,
        }
       
        setShoppingList([...shoppingList, addedProduct]);
        setTotalPrice(product.price + totalPrice);
    }    


    return ( 
        <div className="Product">
            <img src={product.image} alt={product.name}/>
            <p>{product.name}</p>
            <p>{product.price} €</p>
            <div>
                <span>
                    <button onClick={() => addProductIntoShoppingList(product)}>
                        <img src={addCartIcon} alt="add a product in the cart icon"/>  
                    </button>
                </span>
            </div>
        </div>
     );
}
export default Product;