import React, { useContext } from 'react';
import './Navbar.scss';
import { TotalPriceContext } from '../../contexts/TotalPriceContext';


const Navbar = () => {
    const [totalPrice] = useContext(TotalPriceContext);

    return ( 
        <nav className="Navbar">
            <h1>Digifood</h1>
            <button>Payer {totalPrice} €</button>
        </nav>
     );
}
 
export default Navbar;