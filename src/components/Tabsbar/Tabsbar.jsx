import React, { useContext } from 'react';
import { ProductContext } from '../../contexts/ProductContext';
import { ActivatedTabContext } from '../../contexts/ActivatedTabContext';
import './Tabsbar.scss';



const Tabsbar = () => {
    const [productLists] = useContext(ProductContext);
    const [activatedTab, setActivatedTab] = useContext(ActivatedTabContext);

    const showTabContent = category => {
        setActivatedTab(category);
        return activatedTab;
    }

    return ( 
        <nav className="Tabsbar">
            {productLists.categories.map((category, index) =>
                <button key={index} onClick={() => showTabContent(category.name)}>{category.name}</button>
            )}
        </nav>
     );
}
 
export default Tabsbar;