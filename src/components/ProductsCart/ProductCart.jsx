import React, { useContext, useState } from 'react';
import { ShoppingListContext } from '../../contexts/ShoppingListContext';
import { TotalPriceContext } from '../../contexts/TotalPriceContext';
import './ProductCart.scss';
import shopCart from '../../assets/SVGIcons/shopping_cart-24px.svg';
import close from '../../assets/SVGIcons/close-24px.svg';


const ProductCart = () => {
    const [shoppingList, setShoppingList] = useContext(ShoppingListContext);
    const [showShoppingList, setShowShoppingList] = useState(false);
    const [totalPrice, setTotalPrice] = useContext(TotalPriceContext);

    const getAnEmptyCart = () => {
        setShoppingList([]);
        setTotalPrice(0);
        setShowShoppingList(false);
    }

    const removeProductFromShoppingList = (product, shopId) => {
        const updatedShoppingList = [...shoppingList];

        const productToDelete = updatedShoppingList.findIndex(product => product.shopId === shopId);
        updatedShoppingList.splice(productToDelete, 1);
        setShoppingList(updatedShoppingList);

        const total = totalPrice - product.price;
        setTotalPrice(total); 
    }


    const toShowShoppingList = () => {
        if (showShoppingList === true) {
            setShowShoppingList(false);
        } 
        else if (showShoppingList === false) {
            setShowShoppingList(true);
        }
    }

    return (   
        <div>
            { shoppingList.length > 0 &&
            <div className="Cart">       
                { shoppingList.length > 0 && showShoppingList ? (
                    <div className="Cart__shoppingList">
                        <button onClick={() => getAnEmptyCart()}>Supprimer mon panier</button>
                        <button className="Cart__button-close" onClick={toShowShoppingList}>
                            <span>
                                <img src={shopCart} alt=""/>
                                {shoppingList.length}
                            </span>
                        </button>

                        <table>
 
                            {shoppingList.map(product =>
                            <tr key={product.shopId}>

                                <td>{product.name}</td>
                                <td>{product.price} €</td>
                                <td>
                                    <button onClick={() => removeProductFromShoppingList(product, product.id, product.shopId)}>
                                        <img src={close} alt=""/>
                                    </button>
                                </td>
                            </tr>
                            )}
                            
                        </table>
                    </div>
                ) : '' }
                { shoppingList.length > 0 && !showShoppingList ? (
                    <button className="Cart__button" onClick={toShowShoppingList}>
                        <span>
                            <img src={shopCart} alt=""/>
                            {shoppingList.length}
                        </span>
                    </button> 
                ) : ''}
            </div>
            }   
        </div>
    );
}
 
export default ProductCart;