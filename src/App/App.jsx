import React from 'react';
import './App.scss';

import { ProductProvider } from '../contexts/ProductContext';
import { ShoppingListProvider } from '../contexts/ShoppingListContext';
import { TotalPriceProvider } from '../contexts/TotalPriceContext';

import Navbar from '../components/Navbar/Navbar';
import ProductList from '../components/ProductList/ProductList';
import ProductCart from '../components/ProductsCart/ProductCart';
import { ActivatedTabProvider } from '../contexts/ActivatedTabContext';
import Tabsbar from '../components/Tabsbar/Tabsbar';

function App() {
  return (
    <ShoppingListProvider>
    <TotalPriceProvider>
    <ProductProvider>

      <div className="App"> 
        <ActivatedTabProvider>
          <header className="App__header">
            <Navbar/>
            <Tabsbar/>
          </header>   
          <div className="App__content">
            <ProductList/>
          </div>
        </ActivatedTabProvider> 
        <footer className="App__footer">
          <ProductCart/>
        </footer>
      </div>
      
    </ProductProvider>
    </TotalPriceProvider>
    </ShoppingListProvider>
  );
}

export default App;
