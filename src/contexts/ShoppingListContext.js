import React, { createContext, useState } from 'react';

export const ShoppingListContext = createContext();

export const ShoppingListProvider = props => {
    const [shoppingList, setShoppingList] = useState([]);

    return (
        <ShoppingListContext.Provider value={[shoppingList, setShoppingList]}>
            {props.children}
        </ShoppingListContext.Provider>
    )
}