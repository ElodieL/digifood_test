import React, { createContext, useState } from 'react';
import ProductData from '../data/ProductData';

const productData = [ProductData];


export const ActivatedTabContext = createContext();

export const ActivatedTabProvider = props => {
    const [activatedTab, setActivatedTab] = useState(productData[0].categories[0].name)

    return (
        <ActivatedTabContext.Provider value={[activatedTab, setActivatedTab]}>
           {props.children}
        </ActivatedTabContext.Provider>

    )
}