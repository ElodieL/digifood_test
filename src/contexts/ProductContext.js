import React, { createContext, useState } from 'react';
import ProductData from '../data/ProductData';

const productData = [ProductData];

export const ProductContext = createContext();

export const ProductProvider = props => {
    const [productLists, setProductLists] = useState(productData[0]);

    return (
        <ProductContext.Provider value = {[productLists, setProductLists]}>
            {props.children}
        </ProductContext.Provider>
    );
};